package com.vayapay.poc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class PocApplication

fun main(vararg args: String) {
    runApplication<PocApplication>(*args)
}