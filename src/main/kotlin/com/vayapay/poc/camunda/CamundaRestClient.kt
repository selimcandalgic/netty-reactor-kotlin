package com.vayapay.poc.camunda

import kotlinx.coroutines.reactor.awaitSingle
import org.camunda.bpm.engine.rest.dto.VariableValueDto
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.sleuth.Tracer
import org.springframework.http.HttpStatus
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

private const val PROCESS_DEFINITION_KEY = "process-definition/key"
const val TRACE_ID = "B3-TraceId"
const val SPAN_ID = "B3-SpanId"
const val PARENT_ID = "B3-ParentId"
const val BAGGAGE_PREFIX = "B3-Bag-"

@Component
class CamundaRestClient(
    @Autowired webClientBuilder: WebClient.Builder,
    @Autowired private val tracer: Tracer,
) {
    val webClient: WebClient = webClientBuilder.clientConnector(
        ReactorClientHttpConnector(
            HttpClient.create()
                .wiretap(true)
        )
    ).baseUrl("http://localhost:8080/engine-rest/").build()

    suspend fun startProcessInstance(key: String, variables: Map<String, Any>): HttpStatus {
        val startProcessInstanceDto = StartProcessInstanceDto()
        startProcessInstanceDto.variables = convert(variables)

        return webClient.post()
            .uri("$PROCESS_DEFINITION_KEY/$key/start")
            .bodyValue(startProcessInstanceDto)
            .retrieve().toBodilessEntity().awaitSingle().statusCode
    }

    private fun convert(params: Map<String, Any>): Map<String, VariableValueDto> {
        val result = HashMap<String, VariableValueDto>()

        // Add tracing parameters
        tracer.currentSpan()?.let {
            val className = String::class.java.simpleName
            result[TRACE_ID] = VariableValueDto().apply {
                type = className
                value = it.context().traceId()
            }
            result[SPAN_ID] = VariableValueDto().apply {
                type = className
                value = it.context().spanId()
            }
            result[PARENT_ID] = VariableValueDto().apply {
                type = className
                value = it.context().parentId()
            }

            tracer.allBaggage.entries.forEach {
                result["$BAGGAGE_PREFIX${it.key}"] = VariableValueDto().apply {
                    type = className
                    value = it.value
                }
            }
        }

        params.map {
            result[it.key] = VariableValueDto().apply {
                type = it.value.javaClass.simpleName
                value = it.value
            }
        }
        return result
    }
}