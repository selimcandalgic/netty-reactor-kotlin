package com.vayapay.poc.component

import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription
import org.camunda.bpm.client.task.ExternalTask
import org.camunda.bpm.client.task.ExternalTaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.sleuth.Tracer
import org.springframework.stereotype.Component

@Component
@ExternalTaskSubscription(
    topicName = "pocTopic",
    processDefinitionKey = "Poc_Process"
)
class POCTaskHandler(
    @Autowired val tracer: Tracer
) : AbstractTaskHandler(tracer) {

    override suspend fun doTask(externalTask: ExternalTask, externalTaskService: ExternalTaskService) {
        logger.info { "fetched external task : $externalTask" }
        externalTaskService.complete(externalTask)
        logger.info { "completed : $externalTask" }
    }
}