package com.vayapay.poc.component

import com.vayapay.poc.camunda.BAGGAGE_PREFIX
import com.vayapay.poc.camunda.PARENT_ID
import com.vayapay.poc.camunda.SPAN_ID
import com.vayapay.poc.camunda.TRACE_ID
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.slf4j.MDCContext
import mu.KotlinLogging
import org.camunda.bpm.client.task.ExternalTask
import org.camunda.bpm.client.task.ExternalTaskHandler
import org.camunda.bpm.client.task.ExternalTaskService
import org.springframework.cloud.sleuth.Tracer
import org.springframework.cloud.sleuth.instrument.kotlin.asContextElement

abstract class AbstractTaskHandler(private val tracer: Tracer) : ExternalTaskHandler {
    protected val logger = KotlinLogging.logger(this::class.java.name)

    override fun execute(externalTask: ExternalTask?, externalTaskService: ExternalTaskService?) {
        if (externalTask != null && externalTaskService != null) {
            val traceId = externalTask.getVariable<String>(TRACE_ID)
            val spanId = externalTask.getVariable<String>(SPAN_ID)
            val parentId = externalTask.getVariable<String>(PARENT_ID)
            val baggageMap = externalTask.allVariables.filter { it.key.startsWith(BAGGAGE_PREFIX) }.mapKeys {
                it.key.substring(BAGGAGE_PREFIX.length)
            }
            val context = tracer.traceContextBuilder()
                .spanId(spanId)
                .traceId(traceId)
                .parentId(parentId)
                .build()
            val builder = tracer.spanBuilder().setParent(context)
            val span = tracer.withSpan(builder.start())
            baggageMap.forEach {
                tracer.createBaggage(it.key, it.value.toString())
            }
            logger.info { "traceId : $traceId, spanId: $spanId, parentId: $parentId" }
            CoroutineScope(Dispatchers.IO + MDCContext() + tracer.asContextElement()).launch {
                doTask(externalTask, externalTaskService)
            }.invokeOnCompletion {
                span.close()
            }
        }
    }

    abstract suspend fun doTask(externalTask: ExternalTask, externalTaskService: ExternalTaskService)
}