package com.vayapay.poc.conf

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.rsocket.RSocketConnectorConfigurer
import reactor.util.retry.Retry
import java.time.Duration

//
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.reactor.awaitSingleOrNull
//import kotlinx.coroutines.reactor.mono
//import mu.KotlinLogging
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import reactor.core.publisher.Mono
//import reactor.netty.DisposableServer
//import reactor.netty.tcp.TcpServer
//import java.util.concurrent.CompletableFuture
//import kotlin.random.Random
//import kotlin.system.measureTimeMillis
//import kotlin.time.measureTime


@Configuration
class RSocketConfiguration{
    @Bean(name = ["rsocketRetry"])
    fun rSocketConfiguration() = RSocketConnectorConfigurer {
        it.reconnect(Retry.backoff(5, Duration.ofMillis(100)))
    }
}
//
//val logger = KotlinLogging.logger {}
//
//suspend fun doJob(): String {
//    val jobId = Random(System.currentTimeMillis()).nextInt(10_000)
//    logger.info { "doing job $jobId" }
//    delay(10_000)
//    logger.info { "job complete $jobId" }
//    return "done"
//}
//
//suspend fun poc() {
//    Mono.fromCompletionStage(CompletableFuture.runAsync {
//        val time = measureTimeMillis {
//            Thread.sleep(1000)
//            println("hello from ${Thread.currentThread().name}")
//        }
//        println("completed in $time ms.")
//    }).awaitSingleOrNull()
//}
//
//@Configuration
//class PocConfiguration {
//
//    @Bean
//    fun tcpServer(): DisposableServer = TcpServer
//        .create()
//        .handle { _, outbound ->
//            outbound.sendString(
//                mono {
//                    doJob()
//                }
//            )
//        }
//        .host("localhost")
//        .port(1234)
//        .bindNow()
//
//}