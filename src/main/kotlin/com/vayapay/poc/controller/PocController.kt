package com.vayapay.poc.controller

import com.vayapay.poc.camunda.CamundaRestClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.slf4j.MDCContext
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.sleuth.Tracer
import org.springframework.cloud.sleuth.instrument.kotlin.asContextElement
import org.springframework.http.codec.json.KotlinSerializationJsonDecoder
import org.springframework.http.codec.json.KotlinSerializationJsonEncoder
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.stereotype.Controller
import org.springframework.util.MimeType
import org.springframework.util.MimeTypeUtils
import reactor.core.publisher.Hooks
import reactor.core.publisher.Mono
import java.util.concurrent.CancellationException

@Controller
class PocController(
    @Autowired val builder: RSocketRequester.Builder,
    @Autowired private val tracer: Tracer,
    @Autowired private val camundaClient: CamundaRestClient
) {
    val logger = KotlinLogging.logger {}

    init {
        Hooks.onErrorDropped { e: Throwable ->
            if (e is CancellationException || e.cause is CancellationException) {
                logger.trace("Operator called default onErrorDropped", e)
            } else {
                logger.error("Operator called default onErrorDropped", e)
            }
        }
    }

    @MessageMapping("poc")
    fun poc(arg: String): Mono<Boolean> {
        return mono(
            MDCContext() +
                    tracer.asContextElement()
        ) {
            logger.info { "poc is called: $arg, delaying..." }
            delay(2000)
            val requester = builder
                .rsocketStrategies(
                    RSocketStrategies.builder()
                        .decoder(KotlinSerializationJsonDecoder())
                        .encoder(KotlinSerializationJsonEncoder())
                        .build()
                )
                .dataMimeType(MimeType.valueOf(MimeTypeUtils.APPLICATION_JSON_VALUE))
                .tcp("localhost", 8000)
            logger.info { "calling poc1" }
            val result = requester.route("poc1")
                .data("test1")
                .retrieveMono(Boolean::class.java).awaitSingle()
            logger.info { "received response : $result" }
            result
        }
    }

    @MessageMapping("poc1")
    fun poc1(arg: String): Mono<Boolean> {
        return mono(
            MDCContext() +
                    tracer.asContextElement()
        ) {
            logger.info { "poc1 is called: $arg" }
            logger.info { "starting camunda process" }
            camundaClient.startProcessInstance("Poc_Process", emptyMap())
            logger.info { "returning true" }
            true
        }
    }
}

fun <T> monoWithContext(block: suspend CoroutineScope.() -> T?, tracer: Tracer?): Mono<T> {
    val context = if (tracer != null) {
        MDCContext() + tracer.asContextElement()
    } else {
        MDCContext()
    }
    return mono(context) {
        block.invoke(this)
    }
}
