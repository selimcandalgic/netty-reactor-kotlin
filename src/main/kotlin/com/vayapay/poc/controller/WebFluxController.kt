package com.vayapay.poc.controller

import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.slf4j.MDCContext
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.codec.json.KotlinSerializationJsonDecoder
import org.springframework.http.codec.json.KotlinSerializationJsonEncoder
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.util.MimeType
import org.springframework.util.MimeTypeUtils
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class WebFluxController(@Autowired val builder: RSocketRequester.Builder) {
    val logger = KotlinLogging.logger {}

    @PostMapping("/poc")
    fun poc(): Mono<Boolean> {
        val requester = builder
            .rsocketStrategies(
                RSocketStrategies.builder()
                    .decoder(KotlinSerializationJsonDecoder())
                    .encoder(KotlinSerializationJsonEncoder())
                    .build()
            )
            .dataMimeType(MimeType.valueOf(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .tcp("localhost", 8000)
        return mono(MDCContext()) {
            logger.info { "calling poc" }
            val response = requester.route("poc")
                .data("test")
                .retrieveMono(Boolean::class.java).awaitSingle()
            logger.info { "received response : $response" }
            response
        }
    }
}