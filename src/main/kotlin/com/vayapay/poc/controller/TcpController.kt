package com.vayapay.poc.controller

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.slf4j.MDCContext
import kotlinx.coroutines.withContext
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.sleuth.Tracer
import org.springframework.cloud.sleuth.annotation.NewSpan
import org.springframework.cloud.sleuth.instrument.kotlin.asContextElement
import org.springframework.http.codec.json.KotlinSerializationJsonDecoder
import org.springframework.http.codec.json.KotlinSerializationJsonEncoder
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.stereotype.Component
import org.springframework.util.MimeType
import org.springframework.util.MimeTypeUtils

@Component
class TcpController(
    @Autowired builder: RSocketRequester.Builder,
    @Autowired val tracer: Tracer
) {
    val logger = KotlinLogging.logger {}
    private val requester: RSocketRequester

    init {
        builder
            .rsocketStrategies(
                RSocketStrategies.builder()
                    .decoder(KotlinSerializationJsonDecoder())
                    .encoder(KotlinSerializationJsonEncoder())
                    .build()
            )
            .dataMimeType(MimeType.valueOf(MimeTypeUtils.APPLICATION_JSON_VALUE))
        requester = builder.tcp("localhost", 8000)
    }

    @NewSpan
    fun handleRequest(ptoId: String) {
        tracer.createBaggage("ptoId", ptoId)
        runBlocking {
            withContext(
                MDCContext() +
                        Dispatchers.IO +
                        tracer.asContextElement()
            ) {
                logger.info { "received message" }
                logger.info { "calling poc from tcpServer" }
                val response = requester.route("poc")
                    .data("test")
                    .retrieveMono(Boolean::class.java).awaitSingle()
                logger.info { "received response : $response" }
            }
        }

    }
}