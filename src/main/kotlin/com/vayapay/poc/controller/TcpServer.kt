package com.vayapay.poc.controller

import io.netty.bootstrap.ServerBootstrap
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelInitializer
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.rsocket.core.RSocketConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.messaging.rsocket.RSocketConnectorConfigurer
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import reactor.util.retry.Retry
import javax.annotation.PostConstruct

@Component
@ConditionalOnMissingBean(name = ["rsocketRetry"])
class DefaultConfiguration : RSocketConnectorConfigurer {
    override fun configure(connector: RSocketConnector) {
        connector.reconnect(Retry.indefinitely())
    }
}

@Service
class TcpServer(
    @Autowired val controller: TcpController
) {


    @PostConstruct
    fun initialize() {
        val group = NioEventLoopGroup()
        val bootstrap = ServerBootstrap()
        bootstrap.group(group)
            .channel(NioServerSocketChannel::class.java)
            .childHandler(object : ChannelInitializer<SocketChannel>() {
                override fun initChannel(ch: SocketChannel) {
                    ch.pipeline().addLast(object : ChannelInboundHandlerAdapter() {
                        override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
                            (msg as ByteBuf).release()
                            controller.handleRequest("pto1")
                        }
                    })
                }
            })
        bootstrap.bind(1234).sync()
    }
}