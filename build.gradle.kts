import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val camundaVersion: String by project
val xmlBindVersion: String by project
val kotlinLoggingVersion: String by project
val logstashEncoderVersion: String by project
val coroutinesVersion: String by project
val kotlinSerializationVersion: String by project
val nettyVersion: String by project
val springCloudVersion: String by project

plugins {
    kotlin("jvm") version "1.6.10"
    id("org.springframework.boot") version "2.6.8"
    id("org.jetbrains.kotlin.plugin.spring") version "1.6.10"
}
apply(plugin = "io.spring.dependency-management")

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

repositories {
    mavenCentral()
}
fun isM1MacOs() =
    System.getProperty("os.arch").toLowerCase() == "aarch64"
            && System.getProperty("os.name").split(" ")[0].toLowerCase() == "mac"

configure<DependencyManagementExtension> {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
    }
}
dependencies {
    implementation(platform(kotlin("bom")))
    implementation(platform("org.jetbrains.kotlinx:kotlinx-coroutines-bom:$coroutinesVersion"))
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-rsocket")
    implementation("io.projectreactor.netty:reactor-netty-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    implementation("org.springframework.cloud:spring-cloud-starter-sleuth")
    implementation("org.springframework.cloud:spring-cloud-sleuth-zipkin")

    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-external-task-client:$camundaVersion")
    implementation("org.camunda.bpm:camunda-engine-rest-core:$camundaVersion")
    implementation("org.camunda.bpm:camunda-engine:$camundaVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-slf4j")
    if (isM1MacOs()) {
        runtimeOnly("io.netty:netty-resolver-dns-native-macos:$nettyVersion:osx-aarch_64")
    }
    runtimeOnly("net.logstash.logback:logstash-logback-encoder:$logstashEncoderVersion")
    runtimeOnly("org.codehaus.janino:janino")
    runtimeOnly("jakarta.xml.bind:jakarta.xml.bind-api:$xmlBindVersion")
}
